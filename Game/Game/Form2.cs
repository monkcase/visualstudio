﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class Form2 : Form
    {
        private Form1 mainMenuGame;

        public Form2()
        {
            InitializeComponent();
        }

        public Form2(Form1 f1)
        {
            this.mainMenuGame = f1;
            InitializeComponent();

            if (this.mainMenuGame.skillLevel == "easy")
            {
                this.radioButton1.Checked = true;
            }

            if (this.mainMenuGame.skillLevel == "hard")
            {
                this.radioButton2.Checked = true;

            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            this.mainMenuGame.skillLevel = "easy";
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            this.mainMenuGame.skillLevel = "hard";
        }

        private void closedEventForm(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            this.mainMenuGame.Show();
        }
    }
}
