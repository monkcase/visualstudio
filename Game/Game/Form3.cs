﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class Form3 : Form
    {
        private Form1 mainMenuGame;
        private int correct = 0;
        private int incorrect = 0;
        private int taskNumber = 1;
        private string[] expression;
        public Form3()
        {
            InitializeComponent();
        }
        public Form3(Form1 f1)
        {
            this.mainMenuGame = f1;
            InitializeComponent();
            this.setOptionsGame();
            this.generateLevel();
        }

        private void generateLevel()
        {
            Random random = new Random();
            if(this.mainMenuGame.skillLevel == "easy")
            {
                int randomNumber = random.Next(2, 4);
                this.expression = new string[randomNumber+randomNumber-1];
            }
            else
            {
                int randomNumber = random.Next(3, 6);
                this.expression = new string[randomNumber + randomNumber - 1];
            }

            int count = 0;
            foreach (string ch in this.expression)
            {
                if (((count+1) % 2) == 0) {
                    if (random.Next(1, 3) == 1)
                    {
                        this.expression[count] = "+";
                    }
                    else
                    {
                        this.expression[count] = "-";
                    }
                }
                else
                {
                    this.expression[count] = Convert.ToString(random.Next(1, 10));
                }
                count++;
            }
            
            this.label5.Text = String.Join(" ", this.expression);
        }
        private void checkResponse()
        {
            int result = 0;
            int count = 0;
            string sign = "";
            foreach (string ch in this.expression)
            {
                if (count == 0)
                {
                    result = Convert.ToInt32(ch);
                }
                else
                {
                    if (ch == "+") {
                        sign = "+";
                    }
                    else if(ch == "-")
                    {
                        sign = "-";
                    }
                    else
                    {
                        if (sign == "+") {
                            result = result + Convert.ToInt32(ch);
                        } else if (sign == "-")
                        {
                            result = result - Convert.ToInt32(ch);
                        }
                    }
                    
                }
                count++;
            }

            if (this.textBox1.Text != "" && result == Convert.ToInt32(this.textBox1.Text))
            {
                this.correct++;
            }
            else
            {
                this.incorrect++;
            }
        }

        private void setOptionsGame()
        {
            this.label2.Text = "Задача: " + Convert.ToString(this.taskNumber);
            this.label3.Text = "Верно: " + Convert.ToString(this.correct);
            this.label4.Text = "Неверно: " + Convert.ToString(this.incorrect);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.taskNumber++;
            this.checkResponse();
            this.setOptionsGame();
            this.generateLevel();
        }

        private void closedFormEvent(object sender, FormClosedEventArgs e)
        {
            this.mainMenuGame.Show();
        }
    }
}
