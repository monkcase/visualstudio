﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace crestNoll
{
    public partial class Form2 : Form
    {
        private Form1 menuGameAplication;
        private string enemyMode = "computer";
        private string player = "player1";
        private Button[,] buttons;
        
        public Form2()
        {
            InitializeComponent();
        }

        public Form2(Form1 f1, string enemyMode)
        {
            InitializeComponent();
            this.menuGameAplication = f1;
            this.enemyMode = enemyMode;
            this.buttons = new Button[this.menuGameAplication.sizePole, this.menuGameAplication.sizePole];
            for (int i = 0; i < this.menuGameAplication.sizePole; i++)
            {
                for (int j = 0; j < this.menuGameAplication.sizePole; j++)
                {
                    buttons[i, j] = new Button();
                    buttons[i, j].Size = new Size(135, 123);
                }
            }

            this.setButtons();
            this.label2.Text = this.player;
        }

        private void setButtons() {
            for (int i = 0; i < this.menuGameAplication.sizePole; i++)
            {
                for (int j = 0; j < this.menuGameAplication.sizePole; j++)
                {
                    buttons[i, j].Location = new Point(12 + 135 * i, 12 + 123 * j);
                    buttons[i, j].Click += button1_Click;
                    buttons[i, j].Font = new Font(new FontFamily("Microsoft Sans Serif"), 100);
                    this.Controls.Add(buttons[i, j]);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.player == "player1")
            {
                sender.GetType().GetProperty("Text").SetValue(sender, "X");
                if (this.enemyMode == "player2")
                {
                    this.player = this.enemyMode;
                    this.label2.Text = this.player;
                } else if (this.enemyMode == "computer") {

                    this.label2.Text = this.enemyMode;
                    this.computerMove();
                    this.checkGameWin();
                    this.label2.Text = this.player;
                }

            } else if (this.player == "player2") {
                sender.GetType().GetProperty("Text").SetValue(sender, "O");
                this.player = "player1";
                this.label2.Text = this.player;
            }

            sender.GetType().GetProperty("Enabled").SetValue(sender, false);
            this.checkGameWin();
        }

        private void computerMove() {
            Random rnd = new Random();
            List<List<int>> freeValGame = new List<List<int>>();

            for (int i = 0; i < this.menuGameAplication.sizePole; i++) {
                for (int j = 0; j < this.menuGameAplication.sizePole; j++) {
                    if (this.buttons[i, j].Text == "X" || this.buttons[i, j].Text == "O") {
                    }
                    else if (this.buttons[i, j].Text == "") {
                        freeValGame.Add(new List<int>());
                        freeValGame[freeValGame.Count - 1].Add(i);
                        freeValGame[freeValGame.Count - 1].Add(j);
                    }
                }
            }
            int chooseElement = rnd.Next(0, freeValGame.Count - 1);

            if (this.buttons[freeValGame[chooseElement][0], freeValGame[chooseElement][1]].Text == "")
            {
                this.buttons[freeValGame[chooseElement][0], freeValGame[chooseElement][1]].Text = "O";
            }
            
        }

        private void checkGameWin() {
            if (this.menuGameAplication.sizePole == 3) {
                if ((this.buttons[0, 0].Text == "X" && this.buttons[0, 1].Text == "X" && this.buttons[0, 2].Text == "X") || (this.buttons[1, 0].Text == "X" && this.buttons[1, 1].Text == "X" && this.buttons[1, 2].Text == "X") || (this.buttons[2, 0].Text == "X" && this.buttons[2, 1].Text == "X" && this.buttons[2, 2].Text == "X"))
                {
                    MessageBox.Show("X is win!");
                    this.resetGame();
                }
                else if ((this.buttons[0, 0].Text == "X" && this.buttons[1, 0].Text == "X" && this.buttons[2, 0].Text == "X") || (this.buttons[0, 1].Text == "X" && this.buttons[1, 1].Text == "X" && this.buttons[2, 1].Text == "X") || (this.buttons[0, 2].Text == "X" && this.buttons[1, 2].Text == "X" && this.buttons[2, 2].Text == "X"))
                {
                    MessageBox.Show("X is win!");
                    this.resetGame();
                }
                else if ((this.buttons[0, 0].Text == "X" && this.buttons[1, 1].Text == "X" && this.buttons[2, 2].Text == "X") || (this.buttons[0, 2].Text == "X" && this.buttons[1, 1].Text == "X" && this.buttons[2, 0].Text == "X"))
                {
                    MessageBox.Show("X is win!");
                    this.resetGame();
                }
                else if ((this.buttons[0, 0].Text == "O" && this.buttons[0, 1].Text == "O" && this.buttons[0, 2].Text == "O") || (this.buttons[1, 0].Text == "O" && this.buttons[1, 1].Text == "O" && this.buttons[1, 2].Text == "O") || (this.buttons[2, 0].Text == "O" && this.buttons[2, 1].Text == "O" && this.buttons[2, 2].Text == "O"))
                {
                    MessageBox.Show("O is win!");
                    this.resetGame();
                }
                else if ((this.buttons[0, 0].Text == "O" && this.buttons[1, 0].Text == "O" && this.buttons[2, 0].Text == "O") || (this.buttons[0, 1].Text == "O" && this.buttons[1, 1].Text == "O" && this.buttons[2, 1].Text == "O") || (this.buttons[0, 2].Text == "O" && this.buttons[1, 2].Text == "O" && this.buttons[2, 2].Text == "O"))
                {
                    MessageBox.Show("O is win!");
                    this.resetGame();
                }
                else if ((this.buttons[0, 0].Text == "O" && this.buttons[1, 1].Text == "O" && this.buttons[2, 2].Text == "O") || (this.buttons[0, 2].Text == "O" && this.buttons[1, 1].Text == "O" && this.buttons[2, 0].Text == "O"))
                {
                    MessageBox.Show("O is win!");
                    this.resetGame();
                }
            } else if (this.menuGameAplication.sizePole == 5) {
                if ((this.buttons[0, 0].Text == "X" && this.buttons[0, 1].Text == "X" && this.buttons[0, 2].Text == "X" && this.buttons[0, 3].Text == "X" && this.buttons[0, 4].Text == "X") || (this.buttons[1, 0].Text == "X" && this.buttons[1, 1].Text == "X" && this.buttons[1, 2].Text == "X" && this.buttons[1, 3].Text == "X" && this.buttons[1, 4].Text == "X") || (this.buttons[2, 0].Text == "X" && this.buttons[2, 1].Text == "X" && this.buttons[2, 2].Text == "X" && this.buttons[2, 3].Text == "X" && this.buttons[2, 4].Text == "X") || (this.buttons[3, 0].Text == "X" && this.buttons[3, 1].Text == "X" && this.buttons[3, 2].Text == "X" && this.buttons[3, 3].Text == "X" && this.buttons[3, 4].Text == "X") || (this.buttons[4, 0].Text == "X" && this.buttons[4, 1].Text == "X" && this.buttons[4, 2].Text == "X" && this.buttons[4, 3].Text == "X" && this.buttons[4, 4].Text == "X"))
                {
                    MessageBox.Show("X is win!");
                    this.resetGame();
                }
                else if ((this.buttons[0, 0].Text == "X" && this.buttons[1, 0].Text == "X" && this.buttons[2, 0].Text == "X" && this.buttons[3, 0].Text == "X" && this.buttons[4, 0].Text == "X") || (this.buttons[0, 1].Text == "X" && this.buttons[1, 1].Text == "X" && this.buttons[2, 1].Text == "X" && this.buttons[3, 1].Text == "X" && this.buttons[4, 1].Text == "X") || (this.buttons[0, 2].Text == "X" && this.buttons[1, 2].Text == "X" && this.buttons[2, 2].Text == "X" && this.buttons[3, 2].Text == "X" && this.buttons[4, 2].Text == "X") || (this.buttons[0, 3].Text == "X" && this.buttons[1, 3].Text == "X" && this.buttons[2, 3].Text == "X" && this.buttons[3, 3].Text == "X" && this.buttons[4, 3].Text == "X") || (this.buttons[0, 4].Text == "X" && this.buttons[1, 4].Text == "X" && this.buttons[2, 4].Text == "X" && this.buttons[3, 4].Text == "X" && this.buttons[4, 4].Text == "X"))
                {
                    MessageBox.Show("X is win!");
                    this.resetGame();
                }
                else if ((this.buttons[0, 0].Text == "X" && this.buttons[1, 1].Text == "X" && this.buttons[2, 2].Text == "X" && this.buttons[3, 3].Text == "X" && this.buttons[4, 4].Text == "X") || (this.buttons[0, 4].Text == "X" && this.buttons[1, 3].Text == "X" && this.buttons[2, 2].Text == "X" && this.buttons[3, 1].Text == "X" && this.buttons[4, 0].Text == "X"))
                {
                    MessageBox.Show("X is win!");
                    this.resetGame();
                }
                else if ((this.buttons[0, 0].Text == "O" && this.buttons[0, 1].Text == "O" && this.buttons[0, 2].Text == "O" && this.buttons[0, 3].Text == "O" && this.buttons[0, 4].Text == "O") || (this.buttons[1, 0].Text == "O" && this.buttons[1, 1].Text == "O" && this.buttons[1, 2].Text == "O" && this.buttons[1, 3].Text == "O" && this.buttons[1, 4].Text == "O") || (this.buttons[2, 0].Text == "O" && this.buttons[2, 1].Text == "O" && this.buttons[2, 2].Text == "O" && this.buttons[2, 3].Text == "O" && this.buttons[2, 4].Text == "O") || (this.buttons[3, 0].Text == "X" && this.buttons[3, 1].Text == "O" && this.buttons[3, 2].Text == "O" && this.buttons[3, 3].Text == "O" && this.buttons[3, 4].Text == "O") || (this.buttons[4, 0].Text == "O" && this.buttons[4, 1].Text == "O" && this.buttons[4, 2].Text == "O" && this.buttons[4, 3].Text == "O" && this.buttons[4, 4].Text == "O"))
                {
                    MessageBox.Show("O is win!");
                    this.resetGame();
                }
                else if ((this.buttons[0, 0].Text == "O" && this.buttons[1, 0].Text == "O" && this.buttons[2, 0].Text == "O" && this.buttons[3, 0].Text == "O" && this.buttons[4, 0].Text == "O") || (this.buttons[0, 1].Text == "O" && this.buttons[1, 1].Text == "O" && this.buttons[2, 1].Text == "O" && this.buttons[3, 1].Text == "O" && this.buttons[4, 1].Text == "O") || (this.buttons[0, 2].Text == "O" && this.buttons[1, 2].Text == "O" && this.buttons[2, 2].Text == "O" && this.buttons[3, 2].Text == "O" && this.buttons[4, 2].Text == "O") || (this.buttons[0, 3].Text == "X" && this.buttons[1, 3].Text == "O" && this.buttons[2, 3].Text == "O" && this.buttons[3, 3].Text == "O" && this.buttons[4, 3].Text == "O") || (this.buttons[0, 4].Text == "O" && this.buttons[1, 4].Text == "O" && this.buttons[2, 4].Text == "O" && this.buttons[3, 4].Text == "O" && this.buttons[4, 4].Text == "O"))
                {
                    MessageBox.Show("O is win!");
                    this.resetGame();
                }
                else if ((this.buttons[0, 0].Text == "O" && this.buttons[1, 1].Text == "O" && this.buttons[2, 2].Text == "O" && this.buttons[3, 3].Text == "O" && this.buttons[4, 4].Text == "O") || (this.buttons[0, 4].Text == "O" && this.buttons[1, 3].Text == "O" && this.buttons[2, 2].Text == "O" && this.buttons[3, 1].Text == "O" && this.buttons[4, 0].Text == "O"))
                {
                    MessageBox.Show("O is win!");
                    this.resetGame();
                }
            }
        }

        private void closedFormEvent(object sender, FormClosedEventArgs e)
        {
            this.menuGameAplication.Show();
        }
        private void resetGame()
        {
            this.player = "player1";
            for (int i = 0; i < this.menuGameAplication.sizePole; i++)
            {
                for (int j = 0; j < this.menuGameAplication.sizePole; j++)
                {
                    this.buttons[i, j].Text = "";
                }
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.resetGame();
        }
    }
}
