﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace crestNoll
{
    public partial class Form3 : Form
    {
        private Form1 menuGameAplication;
        public Form3()
        {
            InitializeComponent();
        }
        public Form3(Form1 f1)
        {
            InitializeComponent();
            this.menuGameAplication = f1;

            if(this.menuGameAplication.sizePole == 3)
            {
                this.radioButton1.Checked = true;
            } else if(this.menuGameAplication.sizePole == 5)
            {
                this.radioButton2.Checked = true;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            this.menuGameAplication.sizePole = 3;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            this.menuGameAplication.sizePole = 5;
        }

        private void closeFormEvent(object sender, FormClosedEventArgs e)
        {
            this.menuGameAplication.Show();
        }
    }
}
